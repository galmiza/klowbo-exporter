#######################
# Blender GNG exporter
# Copyright 2014
# galmiza@gmail.com
#######################
#
# Overview
# --------
# This script exports a Blender scene into a JSON file structure
# Following objects are exported:
#  mesh (geometry), shape (physic), renderable (transformation+mesh+material+physic+lod_lvl)
#  points
# 
# Following objects are not exported
#  constraints (physic), collision groups (physic)
# 
# Following objects will be exported in a newer release
#  camera, lamp, spline, ...and more
#
#
# Implementation note
# -------------------
# This export script is not attached to any panel
# It is standalone and can be called once when the scene is ready to be exported
# Mesh in the script corresponds to Blender object.data (thus duplicated objects in Blender share the same data)
# Meshes are exported as geometry (vertices+indices)
# Renderable in the script corresponds to a Blender object (it is associated with a geometry)
# Shape in the script corresponds to a Blender object set as a rigid body (object.rigid_body no null)
# 
# 
# 
# 
# Best practices designing BLender scene for the exporter
# To use the same mesh for several objects, create object links using 'object mode'>'Object'>'Duplicate linked'
# Use parent relationship to use position, scale and rotation relative to another object OR to link a physic shape to an object
# 
# 
# 


# Imports
import os # to get folder from filename
import bpy # blender lib
import json # to produce json easily
import struct # to pack data into binary
import base64 # to convert our data to base64
import mathutils, math # math for matrix operations
import re # regular expressions
from mathutils import Vector,Matrix

# Constants 
FILENAME = bpy.path.basename(bpy.data.filepath).replace('.blend', '.json')
DEBUG = 1


# Write a line in the file
def writeString(file, string):
  file.write(bytes(string, 'UTF-8'))

# Pack a list of float/ushort to base64
def writeGeometry(name, data):
  f = open(os.path.dirname(bpy.data.filepath)+'/'+name+'.bin', "wb")
  n = len(data['vertices'])
  f.write(struct.pack('I', n)) # nb vertices as unsigned int
  f.write(struct.pack('f'*n, *data['vertices'])) # vertices (float)
  n = len(data['indices'])
  f.write(struct.pack('I', n)) # nb indices as unsigned int
  f.write(struct.pack('H'*n, *data['indices'])) # indices (unsigned short)
  f.close()

# Convert vectors/quaternions to array and switch [x,y,z] => [x,z,-y]
def Vector3ToArray(v):
  return [v.x,v.z,-v.y]

def Vector3Mul(u,v):
  return Vector((u.x*v.x,u.y*v.y,u.z*v.z))

def Vector4ToArray(v):
  return [v.x,v.z,-v.y,v.w]

def QuaternionToArray(q):
  return [q.x,q.z,-q.y,q.w] # note: moved angle component (w) at the end

# Function from
# https://github.com/jlamarche/iOS-OpenGLES-Stuff/blob/master/Blender%20Export/objc_blend_2.62/io_export_objective_c_header.py
def triangulateMesh(object):
	bneedtri = False # reset flag to request triangulation
	scene = bpy.context.scene # get scene context
	bpy.ops.object.mode_set(mode='OBJECT') # set selection mode
	for i in scene.objects: i.select = False # deselect all objects
	object.select = True # select current object
	scene.objects.active = object # set the mesh object to current
	object.data.update(calc_tessface=True)
	for face in object.data.tessfaces:
		if (len(face.vertices) > 3):
			bneedtri = True # set flag to True if one on the faces has more than 3 vertices
			break
	bpy.ops.object.mode_set(mode='OBJECT') # set selection mode
	if bneedtri == True:
		bpy.ops.object.mode_set(mode='EDIT') # go to edit mode (for current selection)
		bpy.ops.mesh.select_all(action='SELECT') # select all the face/vertex/edge
		bpy.ops.mesh.quads_convert_to_tris() # triangulate selected faces
		bpy.context.scene.update() # apply updates
		bpy.ops.object.mode_set(mode='OBJECT') # go back to object mode
		if DEBUG==1:
                  print("INFO: mesh has been triangulated")

# Define output file
filepath = bpy.path.abspath("//"+FILENAME)

# Prepare arrays for output objects
meshes = []
points = []
shapes = []
renderables = []

# Search on output
def isMeshExported(name):
  for m in meshes:
    if m['name']==name:
      return True
  return False


##################
# Loop on objects
##################
if DEBUG==1:
  print("Found %i objects in current scene" % (len(bpy.context.scene.objects)));
mat_invy = Matrix(((1,0,0,0),(0,-1,0,0),(0,0,1,0),(0,0,0,1))) # inverse y component
for o in bpy.context.scene.objects:
  
  # MESH or SHAPE
  ################
  if o.type == "MESH":
    if o.rigid_body: # ignore if mesh is a shape
      
      # SHAPE
      ########
      if not o.parent: # ignore if not attached to any mesh
        print("WARNING: shape '%s' has no parent. Ignored." % (o.name))
        continue
      if DEBUG==1:
        print("> Exporting shape '%s'..." % (o.name))
      meshes.append({'class': 'Shape', 'name': o.name, 'type': o.rigid_body.collision_shape, 'parent': o.parent.name, 'linear_damping': o.rigid_body.linear_damping, 'angular_damping': o.rigid_body.angular_damping, 'dynamic': o.rigid_body.enabled, 'mass': o.rigid_body.mass, 'friction': o.rigid_body.friction, 'restitution': o.rigid_body.restitution, 'position': Vector3ToArray(o.location), 'orientation': QuaternionToArray(o.rotation_euler.to_quaternion()), 'scale': Vector3ToArray(o.scale*mat_invy)})
    else:
      
      # MESH
      #######
      if not isMeshExported(o.data.name): # export object.data (geometry) if needed
        if DEBUG==1:
          print("> Exporting mesh '%s'..." % (o.data.name))
          print("%i vertices" % (len(o.data.vertices)))
        triangulateMesh(o) # convert quad faces to triangle faces if needed
        mesh = o.to_mesh(bpy.context.scene, False, 'PREVIEW') # get mesh from object
        uv_layer = mesh.uv_layers.active # get access to texture coordinates
        if uv_layer is None:
          if DEBUG==1:
            print("INFO: no uv coordinates for the mesh")
        stride = 10
        vertices = []
        indices = []
        vertex_map = {} # hashmap to detect vertex already defined and reuse its index
        i=0
        for face in mesh.tessfaces: # for each face (3 vertices each)
          for index in face.vertices:
            if len(face.vertices) == 3:
              v = mesh.vertices[index]
              nv = Vector4ToArray(Vector((v.co.x, v.co.y, v.co.z, 1))) # 
              if uv_layer is not None:
                uv = uv_layer.data[i].uv
                nv = nv + [uv.x, uv.y]
              else:
                nv = nv + [0,0]
              if face.use_smooth:
                nv = nv + Vector4ToArray(Vector((v.normal.x, v.normal.y, v.normal.z, 0)))
              else:
                nv = nv + Vector4ToArray(Vector((face.normal.x, face.normal.y, face.normal.z, 0)))
              
              # Calculate index for the vertex
              nv_hash = base64.b64encode(struct.pack('f'*10, *nv))
              if nv_hash in vertex_map:
                indices.append(vertex_map[nv_hash])
              else:
                indices.append(i)
                vertex_map[nv_hash] = i
                vertices = vertices + nv
                i = i+1
        
        # Add mesh as a reference in the json + a binary file
        meshes.append({'class': 'Mesh', 'name': o.data.name})
        writeGeometry(o.data.name, {'vertices': vertices, 'indices': indices})
        
      
      # RENDERABLE
      #############
      if DEBUG==1:
        print("> Exporting renderable '%s'..." % (o.name))
      parent_name = ""
      if o.parent: # has parent => has a relative position with parent (eg. like a tire to its wheel)
        parent_name = o.parent.name;
      
      # Lod
      m = re.match("(\w*?)\.lod(\d*)", o.name, re.M|re.I) # has lod => corresponds to a different lod of an existing renderable
      lod = 0 # lod=0 for the most detailed renderable
      if m:
        lod = int(m.group(2))
        if parent_name == '':
          parent_name = m.group(1)
          if bpy.context.scene.objects[parent_name] is not None:
            print("ERROR: '%s' has to be attached to '%s'" % (o.name,parent_name))
          else:
            print("ERROR: '%s' has an invalid name: '%s' is not an object" % (o.name,parent_name))
      
      # Material
      material = ""
      if len(o.data.materials)>0:
        material = o.data.materials[0].name
      
      # Textures
      textures = []
      for m in o.data.materials:
        for ts in m.texture_slots:
          if ts:
            if ts.texture.type == 'IMAGE':
              if ts.texture.image:
                textures.append(ts.texture.image.name)
              else:
                print("WARNING: no name set for texture of '%s'" % (o.name))
      
      # Prepare output
      r = {'class': 'Renderable', 'name': o.name, 'parent': parent_name, 'lod': lod, 'mesh': o.data.name, 'position': Vector3ToArray(o.location), 'orientation': QuaternionToArray(o.rotation_euler.to_quaternion()), 'scale': Vector3ToArray(o.scale*mat_invy), 'textures': textures, 'material': material }
      if r['material']=="":
        del r['material']
      if r['parent']=="":
        del r['parent']
      renderables.append(r)

  # POINT
  ########
  if o.type == "LAMP":
    parent_name = ""
    if o.parent:
      parent_name = o.parent.name;
    p = {'class': 'Point', 'name': o.name, 'parent': parent_name, 'position': Vector3ToArray(o.location) }
    if p['parent']=="":
      del p['parent']
    points.append(p)

    
  # OTHER
  else:
    if DEBUG==1:
      print("Ignoring %s of type %s..." % (o.name,o.type))


##############
# End of loop
##############

# Writing output
if DEBUG==1:
  print("Opening file %s..." % (filepath));
f = open(filepath, "wb")
writeString(f, json.dumps({'objects': meshes+renderables+shapes+points}));

# End of script, closing file
f.close()
